const { RichText, MediaUpload, PlainText } = wp.editor;
const { registerBlockType } = wp.blocks;
const { Button, ToggleControl} = wp.components;

// Import our CSS files
import './style.scss';
import './editor.scss';

registerBlockType('card-block/main', {   
  title: 'RoomListing',
  icon: 'building',
  category: 'common',
  attributes: {
    body: {
      type: 'array',
      source: 'children',
      selector: '.card__body'
    },
    photos:
    {
      type:'array'
    },
    forSell:
    {
      type:'boolean',
      default:'true'
    },
    forRent:
    {
      type:'boolean',
      default:'true'
    },
    forSellPrice: {
      source: 'text',
      selector: '.card__sellPrice'
    },
    forRentPrice: {
      source: 'text',
      selector: '.card__rentPrice'
    },
    bedRoom: {
      source: 'text',
      selector: '.card__bedRoom'
    },
    bathRoom: {
      source: 'text',
      selector: '.card__bathRoom'
    },
    size: {
      source: 'text',
      selector: '.card__size'
    },
    floor: {
      source: 'text',
      selector: '.card__floor'
    },
  },
  edit({ attributes, className, setAttributes }) {

    const getImageButton = (openEvent) => {
     /* if(attributes.imageUrl) {
        return (
          <img 
            src={ attributes.imageUrl }
            onClick={ openEvent }
            className="image"
          />
        );
      }
      else {*/
        return (
          <div className="button-container">
            <Button 
              onClick={ openEvent }
              className="button button-large"
            >
              Pick an image
            </Button>
          </div>
        );
      //}
    };



    return (
      <div className="container">
        <MediaUpload
          onSelect={ media => {  ; 
          setAttributes({photos: media});
        } }
          type="image"
          multiple={true}
          value={ attributes.imageID }
          render={ ({ open }) => getImageButton(open) }
        />
        <RichText
          onChange={ content => setAttributes({ body: content }) }
          value={ attributes.body }
          multiline="p"
          placeholder="Your card text"
          formattingControls={ ['bold', 'italic', 'underline'] }
          isSelected={ attributes.isSelected }
        />
        <div className="container-flex">
          <ToggleControl
            label="For Sell" 
            checked={ attributes.forSell }
            onChange={ v => { 
              setAttributes({forSell: v})
            }} 
            />
            <PlainText
            onChange={ content => setAttributes({ forSellPrice: content }) }
            value={ attributes.forSellPrice }
            placeholder="0"
            />
            THB
        </div>
        <div className="container-flex">
          <ToggleControl
            label="For Rent" 
            checked={ attributes.forRent }
            onChange={ v => { 
              setAttributes({forRent: v})
            }} 
            />
            <PlainText
            onChange={ content => setAttributes({ forRentPrice: content }) }
            value={ attributes.forRentPrice }
            placeholder="0"
            />
            THB Monthly
          </div>

        <div className="container-flex">
            Number of bed room: 
            <PlainText
            onChange={ content => setAttributes({ bedRoom: content }) }
            value={ attributes.bedRoom }
            placeholder="0"
            />
        </div>
        <div className="container-flex">
            Number of bath room:  
            <PlainText
            onChange={ content => setAttributes({ bathRoom: content }) }
            value={ attributes.bathRoom }
            placeholder="0"
            />
        </div>
        <div className="container-flex">
            Size:  
            <PlainText
            onChange={ content => setAttributes({ size: content }) }
            value={ attributes.size }
            placeholder="0"
            />
        </div>
        <div className="container-flex">
            floor:  
            <PlainText
            onChange={ content => setAttributes({ floor: content }) }
            value={ attributes.floor }
            placeholder="0"
            />
        </div>
      </div>
    );

  },

  save({ attributes }) {

    const cardImage = (src, alt) => {
      if(!src) return null;

      if(alt) {
        return (
          <img 
            className="card__image" 
            src={ src }
            alt={ alt }
          /> 
        );
      }
      
      // No alt set, so let's hide it from screen readers
      return (
        <img 
          className="card__image" 
          src={ src }
          alt=""
          aria-hidden="true"
        /> 
      );
    };

    const cardImages = (a) =>{



      if(a)
      {
        if(a.length > 0 )
        {
          let urls = "";
          for(var i =0; i< a.length; i++)
          {
            urls += ';'+a[i].url;
          }
          return  <div className="cardRoomImageClick"><img 
          className="card__image cardDialogClick" 
          src={ a[0].url }
          alt=""
          aria-hidden="true"/> <div className="hiddenData">{urls}</div></div> ;
        }
      }
      else
      {
        return <div></div>
      }
    }

    return (
      <div className="card">
        {
          cardImages(attributes.photos)
        }
        <div className="card__content">
          <div className="card__body">
            { attributes.body }
          </div>
          <div className="card__detail">
          <div>Bed room</div> <div class="card__bedRoom"> { attributes.bedRoom }</div>
          <div>Bath room</div> <div class="card__bathRoom"> { attributes.bathRoom }</div>
          <div>Size</div>  <div class="card__size"> { attributes.size }</div>
          <div>Floor</div> <div><span class="card__floor"> { attributes.floor }</span></div>
          </div>
          <div className="SellRent">
      {attributes.forSell ? [<div>For Sell</div> ,<div className="card__sellPrice">{attributes.forSellPrice}</div>,<div>THB</div>]:  null}
      {attributes.forRent ?  [<div>For Rent</div>, <div className="card__rentPrice">{attributes.forRentPrice}</div>,<div>THB Monthly</div>]:  null}
          </div>
        </div>
      </div>
    );
  }
});


/*        <div className="button-container">
            <Button 
              onClick={ x => { console.log(attributes.photos) }}
              className="button button-large"
            >
              photos url
            </Button>
          </div>*/