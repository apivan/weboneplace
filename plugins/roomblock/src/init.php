<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function roomblock_cgb_block_assets() { // phpcs:ignore
	// Register block styles for both frontend + backend.
	wp_register_style(
		'roomblock-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);

	// Register block editor script for backend.
	wp_register_script(
		'roomblock-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Register block editor styles for backend.
	wp_register_style(
		'roomblock-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	/**
	 * Register Gutenberg block on server-side.
	 *
	 * Register the block on server-side to ensure that the block
	 * scripts and styles for both frontend and backend are
	 * enqueued when the editor loads.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
	 * @since 1.16.0
	 */
	register_block_type(
		'cgb/block-roomblock', array(
			// Enqueue blocks.style.build.css on both frontend & backend.
			'style'         => 'roomblock-cgb-style-css',
			// Enqueue blocks.build.js in the editor only.
			'editor_script' => 'roomblock-cgb-block-js',
			// Enqueue blocks.editor.build.css in the editor only.
			'editor_style'  => 'roomblock-cgb-block-editor-css',
		)
	);
}


// Hook: Block assets.
add_action( 'init', 'roomblock_cgb_block_assets' );





/**
 * Enqueue block editor JavaScript and CSS
*/
function jsforwpblocks_editor_scripts() {
	// Make paths variables so we don't write em twice ;)
	$blockPath = '/assets/js/blocks.js';
	$editorStylePath = '/assets/css/blocks.editor.css';
	// Enqueue the bundled block JS file
	wp_enqueue_script(
	  'jsforwp-blocks-js',
	  plugins_url( $blockPath, __FILE__ ),
	  [  'wp-blocks', 'wp-element', 'wp-components', 'wp-i18n' ],
	  filemtime( plugin_dir_path( __FILE__ ) . $blockPath )
	);
	// Enqueue optional editor only styles
	wp_enqueue_style(
	  'jsforwp-blocks-editor-css',
	  plugins_url($editorStylePath, __FILE__),
	  [  'wp-blocks', 'wp-element', 'wp-components', 'wp-i18n' ],
	  filemtime( plugin_dir_path( __FILE__ ) . $editorStylePath )
	);
  }
  // Hook scripts function into block editor hook
  add_action( 'enqueue_block_editor_assets', 'jsforwpblocks_editor_scripts' );
  /**
   * Enqueue block editor JavaScript and CSS
  */
  function jsforwpblocks_scripts() {
	// Make paths variables so we don't write em twice ;)
	$sharedBlockPath = '/assets/js/blocks.shared.js';
	$stylePath = '/assets/css/blocks.style.css';
	$frontendStylePath = '/assets/css/blocks.frontend.css';

	// Enqueue frontend and editor block styles
	wp_enqueue_style(
	  'jsforwp-blocks-css',
	  plugins_url($stylePath, __FILE__)
	);
	// Enqueue frontend only CSS
	if( !is_admin() ) {
	  wp_enqueue_style(
		'jsforwp-blocks-frontend-css',
		plugins_url($frontendStylePath, __FILE__)
		);
		
	  // Enqueue frontend and editor JS
	  wp_enqueue_script(
		'jsforwp-blocks-frontend-js',
		plugins_url( $sharedBlockPath, __FILE__ ),
		[  'wp-blocks', 'wp-element', 'wp-components', 'wp-i18n'],
		filemtime( plugin_dir_path( __FILE__ ) . $sharedBlockPath )
	  );

	 }
  }
  // Hook scripts function into block editor hook
	add_action( 'enqueue_block_assets', 'jsforwpblocks_scripts' );
	

/* 	function callback_for_setting_up_scripts() {
	// Make paths variables so we don't write em twice ;)
	$stylePath = '/assets/css/blocks.style.css';
	$frontendStylePath = '/assets/css/blocks.frontend.css';

	// Enqueue frontend and editor block styles
	wp_enqueue_style(
	  'jsforwp-blocks-css2',
		plugins_url($stylePath, __FILE__)
	);
	// Enqueue frontend only CSS
	if( !is_admin() ) {
	  wp_enqueue_style(
		'jsforwp-blocks-frontend-css2',
		plugins_url($frontendStylePath, __FILE__)
		);

	 }
	}
	add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts'); */