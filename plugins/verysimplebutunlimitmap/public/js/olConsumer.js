
jQuery ( document ).ready(function() {


    let pinImg = myScript.pinSvg;
    console.log(pinImg);


        var layerEn = new ol.layer.Tile({
          source: new ol.source.OSM({
            opaque: false,
            url: 'https://thaimap.tile.osm-tools.org/osm/{z}/{x}/{y}.png'
          })
        });

        let ele = document.createElement("span");
        ele.innerHTML = 'Uses <a href="https://www.osm-tools.org/">&copy Thaimap.osm-tools</a> <a href="https://www.osm-tools.org/">&copy OpenStreetMap contributors</a>  <a href="https://openlayers.org/">&copy OpenLayers</a>';
        

//https://tile.iosb.fraunhofer.de/tiles/osmde/6/10/10.png
//
/*
      var layerEn = new ol.layer.Tile({
        source: new ol.source.OSM({
          opaque: false,
          url: 'https://tile.iosb.fraunhofer.de/tiles/osmde/{z}/{x}/{y}.png'
        })
      });

        let ele = document.createElement("span");
        ele.innerHTML = 'Uses <a href="https://tile.iosb.fraunhofer.de/">&copy iosb.fraunhofer.de</a> <a href="https://www.osm-tools.org/">&copy OpenStreetMap contributors</a>  <a href="https://openlayers.org/">&copy OpenLayers</a>';
*/

       /* 
        let ele = document.createElement("span");
        ele.innerHTML = 'Uses <a href="https://www.thunderforest.com/">&copy thunderforest</a> <a href="https://www.openstreetmap.org/">&copy OpenStreetMap contributors</a>  <a href="https://openlayers.org/">&copy OpenLayers</a>';

        var layerEn = new ol.layer.Tile({
          source: new ol.source.OSM({
            opaque: false,
            url: 'https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey='+ myScript.thunderforestApikey
          })
        });
        */

      /*
      var layerEn = new ol.layer.Tile({
        source: new ol.source.OSM({
          opaque: false,
          url: 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=en'
        })
      });
      */

      /*  var layer = new  ol.layer.Tile({
            source: new ol.source.OSM()
          });
    */


  
   var iconStyle = new ol.style.Style({
    image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        anchor: [15, 52],
        anchorXUnits: 'pixels',
        anchorYUnits: 'pixels',
        opacity: 1.0,
        src: myScript.pinSmallSvg
    })),
    text: new ol.style.Text({
      font: '12px Calibri,sans-serif',
      fill: new ol.style.Fill({
        color: '#000'
      }),
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 3
      }),
      offsetX:0,
      offsetY:-48

    })

    });

  var highlightStyle = new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
        anchor: [15, 52],
        anchorXUnits: 'pixels',
        anchorYUnits: 'pixels',
        opacity: 1.0,
        src: myScript.pinSmallSvg,
        scale:1.2
      })),

      text: new ol.style.Text({
        font: '12px Calibri,sans-serif',
        fill: new ol.style.Fill({
          color: '#000'
        }),
        stroke: new ol.style.Stroke({
          color: '#ff0',
          width: 3
        }),
        offsetX:0,
        offsetY:-48
      })
    });


    let AddIconInteractive = function(map)
    {

      var featureOverlay = new ol.layer.Vector({
        source: new ol.source.Vector(),
        map: map,
        style: function(feature) {
          highlightStyle.getText().setText(feature.get('name'));
          return highlightStyle;
        }
      });


      let highlight;
      var displayFeatureInfo = function(pixel) {

        var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
          return feature;
        });

        if (feature !== highlight) {
          if (highlight) {
            featureOverlay.getSource().removeFeature(highlight);
          }
          if (feature) {
            featureOverlay.getSource().addFeature(feature);
          }
          highlight = feature;
      }
      return feature;

      };

      map.on('pointermove', function(evt) {
        if (evt.dragging) {
          return;
        }
        var pixel = map.getEventPixel(evt.originalEvent);
        let hit = displayFeatureInfo(pixel);

        if (hit) {
           // this.getTargetElement().focus();
            this.getTargetElement().style.cursor = 'pointer';
        } else {
            this.getTargetElement().style.cursor = '';
        }

      });

      map.on('click', function(evt) {

      //  this.getTargetElement().focus();

        var feature = displayFeatureInfo(evt.pixel);
        if (feature) {
          let url = feature.get('url');
          if(url)
              window.open(url);
             // window.location.href = url;

        }
      });

    };

    let AddViewInteractive = function(view, thisDoc, lon, lat, zoom)
    {
      jQuery( thisDoc )[0].appendChild(ele);
      let selfId = thisDoc.id;
      thisDoc.querySelector( ".ol-zoom-in").addEventListener("click", function(){
        document.getElementById(selfId).focus();
      });
      thisDoc.querySelector( ".ol-zoom-out").addEventListener("click", function(){
        document.getElementById(selfId).focus();
      });

      var focusBtn = document.createElement("button");
      focusBtn.innerHTML = "R";
      focusBtn.addEventListener("click", function(){

        view.animate({
          center: ol.proj.fromLonLat([lon, lat]),
          zoom: zoom,
          duration: 500
        });
        document.getElementById(selfId).focus();
      });
      thisDoc.querySelector( ".ol-zoom.ol-unselectable.ol-control").appendChild(focusBtn);

    }

    jQuery("[data-ol-multimap]").each(function( index ) {
        let lon = parseFloat(jQuery( this ).attr('data-ol-map-lon'));
        let lat = parseFloat(jQuery( this ).attr('data-ol-map-lat'));

        jQuery( this ).attr('tabindex', 0);

          var view =  new  ol.View({
            center: ol.proj.fromLonLat([lon, lat]),
            zoom: 13
          });

          var iconFeatures=[];


          let x = document.querySelectorAll("div.olMarker");

          for (i = 0; i < x.length; i++) {

            let posx = ol.proj.fromLonLat(
              [parseFloat(x[i].getAttribute("data-ol-map-lon")), parseFloat(x[i].getAttribute("data-ol-map-lat"))]);
            let a = x[i].querySelector("a");
              var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point(posx),
                name: a.innerText,
            });
            iconFeature.set("url",a.href);
            iconFeatures.push(iconFeature);
          }



          var vectorSource = new ol.source.Vector({
            features: iconFeatures
          });


          var vectorLayer = new ol.layer.Vector({
              source: vectorSource,
              style: function(feature) {
                iconStyle.getText().setText(feature.get('name'));
                return iconStyle;
              }
          });

          var map = new ol.Map({
            /*interactions: ol.interaction.defaults({
               onFocusOnly: true
            }),*/
            layers: [ layerEn, vectorLayer],
            target: jQuery( this )[0].id,
            view: view,

          });
          AddIconInteractive(map);
          AddViewInteractive(view, this, lon, lat,13);

      });

  
    jQuery("[data-ol-map]").each(function( index ) {
        let lon = parseFloat(jQuery( this ).attr('data-ol-map-lon'));
        let lat = parseFloat(jQuery( this ).attr('data-ol-map-lat'));

        let pin = jQuery( this ).attr('data-ol-map') == "pin";
        jQuery( this ).attr('tabindex', 0);
        let view =  new ol.View({
          center: ol.proj.fromLonLat( [lon,lat]) ,
          zoom: 17
          });
        if(pin)
        {
            var iconFeatures=[];
            let nameq ='';
            let header = document.querySelector("header.entry-header .entry-title");
            if(document.querySelector("header.entry-header .entry-title"))
             nameq = header.innerText;

            var iconFeature = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.fromLonLat( [lon,lat])),
                name: nameq,
            });
            iconFeature.set("url",null);
            iconFeatures.push(iconFeature);


            var vectorSource = new ol.source.Vector({
            features: iconFeatures //add an array of features
            });


            var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: function(feature) {
                  iconStyle.getText().setText(feature.get('name'));
                  return iconStyle;
                }
            });

            map = new ol.Map({
                   /*   interactions: ol.interaction.defaults({
                        onFocusOnly: true
                    }),*/
                    target: jQuery( this )[0].id,
                    layers: [ layerEn, vectorLayer],
                    view: view
                });


            AddIconInteractive(map);
            AddViewInteractive(view, this, lon, lat,17);

        }
        else
        {

            let map = new ol.Map({
               /*   interactions: ol.interaction.defaults({
                    onFocusOnly: true
                }),*/
                target: jQuery( this )[0].id,
                layers: [layerEn],
                view: view
            });
        }


      });
});