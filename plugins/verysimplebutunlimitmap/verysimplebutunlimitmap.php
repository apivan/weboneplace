<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://kfkhunanan.com
 * @since             1.0.0
 * @package           Verysimplebutunlimitmap
 *
 * @wordpress-plugin
 * Plugin Name:       VerySimpleButUnlimitMap
 * Plugin URI:        https://kfkhunanan.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Apivan
 * Author URI:        https://kfkhunanan.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       verysimplebutunlimitmap
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'VERYSIMPLEBUTUNLIMITMAP_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-verysimplebutunlimitmap-activator.php
 */
function activate_verysimplebutunlimitmap() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-verysimplebutunlimitmap-activator.php';
	Verysimplebutunlimitmap_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-verysimplebutunlimitmap-deactivator.php
 */
function deactivate_verysimplebutunlimitmap() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-verysimplebutunlimitmap-deactivator.php';
	Verysimplebutunlimitmap_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_verysimplebutunlimitmap' );
register_deactivation_hook( __FILE__, 'deactivate_verysimplebutunlimitmap' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-verysimplebutunlimitmap.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_verysimplebutunlimitmap() {

	$plugin = new Verysimplebutunlimitmap();
	$plugin->run();

}
run_verysimplebutunlimitmap();


function vsbul_shortcode( $atts ) {
	$a = shortcode_atts( array(
	   'name' => 'world',
	   'feature' => '',
	   'lat' => '0',
	   'lon' => '0'

	), $atts );
//[vsbul_map name="mapsiam" feature="" lat="13.745605" lon="00.534191"]
	return '<div id="'.$a['name'].'" class="map" data-ol-map="'.$a['feature'].'" data-ol-map-lat="'.$a['lat'].'" data-ol-map-lon="'.$a['lon'].'"></div>';
 }

 add_shortcode( 'vsbul_map', 'vsbul_shortcode' );