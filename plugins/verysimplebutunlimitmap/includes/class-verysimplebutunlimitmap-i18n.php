<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://kfkhunanan.com
 * @since      1.0.0
 *
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 * @author     Apivan <apivant@gmail.com>
 */
class Verysimplebutunlimitmap_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'verysimplebutunlimitmap',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
