<?php

/**
 * Fired during plugin activation
 *
 * @link       https://kfkhunanan.com
 * @since      1.0.0
 *
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 * @author     Apivan <apivant@gmail.com>
 */
class Verysimplebutunlimitmap_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
