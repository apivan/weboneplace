<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://kfkhunanan.com
 * @since      1.0.0
 *
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Verysimplebutunlimitmap
 * @subpackage Verysimplebutunlimitmap/includes
 * @author     Apivan <apivant@gmail.com>
 */
class Verysimplebutunlimitmap_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
