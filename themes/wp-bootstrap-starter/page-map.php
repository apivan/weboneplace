<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
	<section id="primary" class="content-area col-sm-12 col-lg-8 customWidth">
		<main id="main" class="site-main hideTitle Mappage" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

			endwhile; // End of the loop.
			?>

<?php
	$choices = get_field('buildinglocations', 5, false);
	
	// explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);	
	// remove any unwanted white space
	$choices = array_map('trim', $choices);

	// loop through array and add to field 'choices'
	if( is_array($choices) ) {

		?>
		<div style="display:none">
		<?php
		$mapindex = 0;
		foreach( $choices as $choice ) {

			if(strpos($choice, ':') !== false)
			{
				$buildingNameP1 = explode(':', $choice);
				if(sizeof($buildingNameP1) > 1)
				{
					
					$buildingNamePX = explode(',', $buildingNameP1[1]);

					if(sizeof($buildingNamePX) > 1)
					{
						$buildingurl = get_site_url()."/".str_replace(" ","-",$buildingNameP1[0]);

						$mapindex++;
						echo "<div  id=\"map$mapindex\" class=\"olMarker\" 
						data-ol-map-name=\"$buildingNameP1[0]\"  
						 data-ol-map-lat=\"$buildingNamePX[0]\" 
						 data-ol-map-lon=\"$buildingNamePX[1]\">
						 <a  href=\"$buildingurl\" ><span>$buildingNameP1[0]</span></a>
						</div>";
					}
				}
			}
		}
		
		

		?>
		</div>
		<div style="
    margin-top: 1rem;
">
<div class="line-it-button" data-lang="en" data-type="share-a" data-ver="3" data-url="https://theoneplacebangkok.com/map" data-color="default" data-size="small" data-count="false" style="display: none;"></div>
 <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
 </div>
		<?php
	}



?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php

if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>