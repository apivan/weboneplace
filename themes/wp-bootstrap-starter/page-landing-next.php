<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header("landing-next"); ?>
	<section id="primary" class="content-area col-sm-12 col-lg-8 customWidth">
		<main id="main" class="site-main hideTitle" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

			endwhile; // End of the loop.
			?>

<?php
	$choices = get_field('buildinglocations', 5, false);
	
	// explode the value so that each line is a new array piece
	$choices = explode("\n", $choices);	
	// remove any unwanted white space
	$choices = array_map('trim', $choices);

	// loop through array and add to field 'choices'
	if( is_array($choices) ) {

		?>
		<div style="display:none">
		<?php
		$mapindex = 0;
		foreach( $choices as $choice ) {

			if(strpos($choice, ':') !== false)
			{
				$buildingNameP1 = explode(':', $choice);
				if(sizeof($buildingNameP1) > 1)
				{
					
					$buildingNamePX = explode(',', $buildingNameP1[1]);

					if(sizeof($buildingNamePX) > 1)
					{
						$buildingurl = get_site_url()."/".str_replace(" ","-",$buildingNameP1[0]);

						$mapindex++;
						echo "<div  id=\"map$mapindex\" class=\"olMarker\" 
						data-ol-map-name=\"$buildingNameP1[0]\"  
						 data-ol-map-lat=\"$buildingNamePX[0]\" 
						 data-ol-map-lon=\"$buildingNamePX[1]\">
						 <a  href=\"$buildingurl\" ><span>$buildingNameP1[0]</span></a>
						</div>";
					}
				}
			}
		}


		// performance test
		$x = 100.462456;

		for($i = 0; $i <= 10; $i++)
		{
			$x += 0.02;
			$y = 13.735879;

			for($j = 0; $j <= 10; $j++)
			{
			
				
			
				$y += 0.01;
		
				echo "<div  id=\"map$mapindex\" class=\"olMarker\" 
				data-ol-map-name=\"Generated\"  
				data-ol-map-lat=\"$y \" 
				data-ol-map-lon=\"$x\">
				<a  href=\"$buildingurl\" ><span>Generated</span></a>
				</div>";
			}
		}


		?>
		</div>
		<?php
	}



?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();

function console_debug( $data ) {
    $data = json_encode($data);
    echo "<script>console.log($data)</script>";
}


if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) {?>
        <div id="footer-widget" class="row m-0 <?php if(!is_theme_preset_active()){ echo 'bg-light'; } ?> site-footer navbar-dark bg-primary">
            <div class="flexFoot col-12">

                <?php
                     get_template_part("footer-widget-generated-tags")

                ?> 
                        
                <div style="flex: 1 0 0;">
                    <div>
                        <?php if ( is_active_sidebar( 'footer-1' )) : ?>
                            <div ><?php dynamic_sidebar( 'footer-1' ); ?></div>
                        <?php endif; ?>
                        <?php if ( is_active_sidebar( 'footer-2' )) : ?>
                            <div ><?php dynamic_sidebar( 'footer-2' ); ?></div>
                        <?php endif; ?>
                        <?php if ( is_active_sidebar( 'footer-3' )) : ?>
                            <div ><?php dynamic_sidebar( 'footer-3' ); ?></div>
                        <?php endif; ?>
                        <h3 class="widget-title">Contract Us</h3>
                        <div class="flex-table">
                            <div class= "table" style="justify-content: start;">
                                <span>Call </span>  <span> <?php echo the_field('phone_number',5)?> </span>
                                <span>Line ID</span> <span> <a href="http://nav.cx/2vKtOWD" > @231kcrwa </a></span>
                                <span style=" grid-column: 1 / span 2;">
                                <a href="mailto:<?php echo the_field('email',5)?>"><?php echo the_field('email',5)?></a></span>
                            </div>

                            Special thank to
                            <div>
                            <a href="<?php echo get_permalink( get_page_by_path( 'developer' ) )?>">The web developer</a> 
                            </div>
                            <span>
                            &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
                            </span>
                        </div >
                    </div>
                </div>
		
            </div>


        </div>

<?php }
