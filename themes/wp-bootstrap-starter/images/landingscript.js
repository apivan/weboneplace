
(function() {
document.querySelectorAll('#headerSceneGroup .scene').forEach((elem) => {
            
    const modifier = elem.getAttribute('data-modifier')
    
    basicScroll.create({
        elem: elem,
        from: 0,
        to: 1000,
        direct: true,
        props: {
            '--translateY': {
                from: '0',
                to: `${ 10 * modifier }px`
            }
        }
    }).start()
        
})


const elem = document.querySelector('#likeHandSvg')
const thumbHand = SVG.adopt(elem).select('#thumb')

const handscroll = basicScroll.create({
elem: elem,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => thumbHand.rotate(props['--deg'], 200, 200),
props: {
    '--deg': {
    from: 50,
    to: -10
    }
}
})
handscroll.start()


const elemPeople = document.querySelector('#peoplehand')
const peopleHand = SVG.adopt(elemPeople).select('#g12')

const peopleHandScroll = basicScroll.create({
elem: elemPeople,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => peopleHand.rotate(props['--deg'], 256, 256),
props: {
    '--deg': {
    from: 180,
    to: 0
    }
}
})
peopleHandScroll.start()


const elemOne = document.querySelector('#onestop')
const onePath = SVG.adopt(elemOne).select('#one')

const oneScroll = basicScroll.create({
elem: elemOne,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => {onePath.opacity(props['--deg']);onePath.scale(props['--deg']);  },
props: {
    '--deg': {
    from: 0.0,
    to: 1.5
    }
}
})
oneScroll.start()


const elemTrust = document.querySelector('#trust')
const trustPath = SVG.adopt(elemTrust).select('#ttop')
const trustleftPath = SVG.adopt(elemTrust).select('#tleft')
const trustrightPath = SVG.adopt(elemTrust).select('#tright')

const trustScroll = basicScroll.create({
elem: elemTrust,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => {
 trustPath.rotate(props['--deg'], 13, 6);
 trustleftPath.translate(0,props['--deg']/(-7));
 trustrightPath.translate(0,props['--deg']/(7));
 },
    props: {
        '--deg': {
        from: -45,
        to: 5
        }
    }
    })
trustScroll.start()


const elemBar = document.querySelector('#rise')
const barArrowPath = SVG.adopt(elemBar).select('#arrow')
const bar1Path = SVG.adopt(elemBar).select('#bar1')
const bar2Path = SVG.adopt(elemBar).select('#bar2')
const bar3Path = SVG.adopt(elemBar).select('#bar3')
const bar4Path = SVG.adopt(elemBar).select('#bar4')
const bar5Path = SVG.adopt(elemBar).select('#bar5')

const barScroll = basicScroll.create({
elem: elemBar,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => {
    barArrowPath.opacity(props['--var1']-3.5);
    bar1Path.opacity(props['--var1']);
    bar2Path.opacity(props['--var1']-1);
    bar3Path.opacity(props['--var1']-2);
    bar4Path.opacity(props['--var1']-3);
    bar5Path.opacity(props['--var1']-3.5);
 },
    props: {
        '--var1': {
        from: 0,
        to: 5
        }
    }
    })
barScroll.start()


const elemTrans = document.querySelector('#transIcon')
const atext = SVG.adopt(elemTrans).select('#bola')
const btext = SVG.adopt(elemTrans).select('#bolb')

const transScroll = basicScroll.create({
elem: elemTrans,
from: 'middle-bottom',
to: 'middle-top',
inside: (instance, percentage, props) => {
    atext.translate(props['--var1'],0);
    btext.translate(props['--var2'],0);
 },
    props: {
        '--var1': {
        from: 80,
        to: -10
        },
        '--var2': {
            from: -1000,
            to: 150
            }
    }
    })
transScroll.start()

})();