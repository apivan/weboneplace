
<nav class="navbar navbar-expand-xl p-0" >
    <div class="navbar-brand">
    <a href="<?php echo get_home_url();?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logohon.svg"/> </a>
    </div>
    <div>
        <a href="tel://<?php echo the_field('phone_number',5); ?>"> 
        <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/smartphone-call.svg"/>--> <span> <?php echo the_field('phone_number',5)?> </span>
        </a>        
        <a class="resp-sharing-button__link" href="https://wa.me/66934671457" target="_blank" rel="noopener" aria-label="" style="margin:auto;margin-left: 1rem;">
          <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.1 3.9C17.9 1.7 15 .5 12 .5 5.8.5.7 5.6.7 11.9c0 2 .5 3.9 1.5 5.6L.6 23.4l6-1.6c1.6.9 3.5 1.3 5.4 1.3 6.3 0 11.4-5.1 11.4-11.4-.1-2.8-1.2-5.7-3.3-7.8zM12 21.4c-1.7 0-3.3-.5-4.8-1.3l-.4-.2-3.5 1 1-3.4L4 17c-1-1.5-1.4-3.2-1.4-5.1 0-5.2 4.2-9.4 9.4-9.4 2.5 0 4.9 1 6.7 2.8 1.8 1.8 2.8 4.2 2.8 6.7-.1 5.2-4.3 9.4-9.5 9.4zm5.1-7.1c-.3-.1-1.7-.9-1.9-1-.3-.1-.5-.1-.7.1-.2.3-.8 1-.9 1.1-.2.2-.3.2-.6.1s-1.2-.5-2.3-1.4c-.9-.8-1.4-1.7-1.6-2-.2-.3 0-.5.1-.6s.3-.3.4-.5c.2-.1.3-.3.4-.5.1-.2 0-.4 0-.5C10 9 9.3 7.6 9 7c-.1-.4-.4-.3-.5-.3h-.6s-.4.1-.7.3c-.3.3-1 1-1 2.4s1 2.8 1.1 3c.1.2 2 3.1 4.9 4.3.7.3 1.2.5 1.6.6.7.2 1.3.2 1.8.1.6-.1 1.7-.7 1.9-1.3.2-.7.2-1.2.2-1.3-.1-.3-.3-.4-.6-.5z"></path></svg>
            </div>
          </div>
        </a>
        
        <a href="http://nav.cx/2vKtOWD"><img src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png" alt="Add friend" height="36" border="0"  style="width: 8rem;padding-left:1rem;"></a>
        <a href="mailto:<?php echo the_field('email',5)?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gmail.svg" alt="email" height="36" border="0"  style="height: 2.5rem;padding-left:1rem;"></a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>    
    <?php
    wp_nav_menu(array(
    'theme_location'    => 'primary',
    'container'       => 'div',
    'container_id'    => 'main-nav',
    'container_class' => 'collapse navbar-collapse justify-content-end',
    'menu_id'         => false,
    'menu_class'      => 'navbar-nav',
    'depth'           => 3,
    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
    'walker'          => new wp_bootstrap_navwalker()
    ));
    ?>

</nav>