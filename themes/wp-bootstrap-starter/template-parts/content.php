<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php wp_bootstrap_starter_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
		if ( is_single() ) :
			
			$isShownMap = get_field( "showmap" );
			if( $isShownMap ) {
				$buildingName = get_field( "buildingname" );

				//echo "<script>console.log('Your stuff here $buildingName')</script>";
				if(strpos($buildingName, ':') !== false)
				{
					$buildingNameP1 = explode(':', $buildingName);
					if(sizeof($buildingNameP1) > 1)
					{

						$buildingNamePX = explode(',', $buildingNameP1[1]);

						if(sizeof($buildingNamePX) > 1)
						{
							echo "<div id=\"$the_ID()\" class=\"map postmap\" data-ol-map=\"pin\" data-ol-map-lat=\"$buildingNamePX[0]\" data-ol-map-lon=\"$buildingNamePX[1]\"></div>";
						}
					}
				}
			}

			the_content();
        else :
            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
        endif;

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
