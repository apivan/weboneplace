<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 * 
 * 
 * !!!Change the resource/EmscriptenSampleOgreCut.js file
 * !!! Use Cpanel to add wasm mime type 
 * 
 */

function console_debug( $data ) {
    $data = json_encode($data);
    echo "<script>console.log($data)</script>";
}

get_header(); 

?>

	<section id="primary" class="content-area col-sm-12 col-lg-8 customWidth">
		<main id="main" class="site-main" role="main">
		<p> Hi, I am the web developer of the one place!</p>
		<p> Here my specialties.</p>
		<ul>
        <li>
          Efficiently dealing with scientific or medical libraries that was written in C++ (98/17), Python, Javascript and other languages
          that dominated these fields
        </li>
        <li>
          Making cross-platform application for Linux, Windows, Android, and OSX with single source code.
        </li>
        <li>
          Creating high performace (web) application, easy to deploy, integrating cutting-edge technology, the webassembly
        </li>
        <li>
		  Proposing intuitive design for touch mobile device, mouse-keybord desktop, website, and post-modern air touch application
        </li>
        <li>Efficiently utilizing of hardware resource and able to run fluidly in constrain platforms like embedded OSes or mobile devices</li>
        <li>
          Appling best practices for development, testing, documentation, tracking bugs, and feedback management
        </li>
		<li>
          Experienced with wordpress. Making wordpress plugins. And, this site is wordpress!
        </li>
	 <!--	<a href="https://www.linkedin.com/in/apivantuntakurn/">Click here to see my linkedin profile</a> -->
      </ul>
			<?php

			// while ( have_posts() ) : the_post();

			// 	get_template_part( 'template-parts/content', 'page' );

            //     // If comments are open or we have at least one comment, load up the comment template.
            //     if ( comments_open() || get_comments_number() ) :
            //         comments_template();
            //     endif;

			// endwhile; // End of the loop.

			?>
 <div class="spinner" id='spinner'></div>
    <div class="emscripten" id="status">Downloading...</div>
    <div class="emscripten">
      <progress value="0" max="100" id="progress" hidden=1></progress>
    </div>
    <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" ></canvas>
    <caption><p>A webassembly program.</p></caption>
<style>
@media screen   {
  #canvas {
    max-width: 1122px;
    min-width: 1122px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
@media screen and (max-width: 1200px)  {
  #canvas {
    max-width: 922px;
    min-width: 922px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
@media screen and (max-width: 992px)   {
  #canvas {
    max-width: 702px;
    min-width: 702px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
@media screen and (max-width: 792px)   {
  #canvas {
	max-width: 592px;
    min-width: 592px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
</style>
<script>
var statusElement = document.getElementById('status');
var progressElement = document.getElementById('progress');
var spinnerElement = document.getElementById('spinner');

var Module = {
  preRun: [],
  postRun: [],
  print: (function() {
   // var element = document.getElementById('output');
   // if (element) element.value = ''; // clear browser cache
    return function(text) {
      if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
      // These replacements are necessary if you render to raw HTML
      //text = text.replace(/&/g, "&amp;");
      //text = text.replace(/</g, "&lt;");
      //text = text.replace(/>/g, "&gt;");
      //text = text.replace('\n', '<br>', 'g');
      console.log(text);
     // if (element) {
    //    element.value += text + "\n";
    //    element.scrollTop = element.scrollHeight; // focus on bottom*/
    //  }
    };
  })(),
  printErr: function(text) {
    if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
    if (0) { // XXX disabled for safety typeof dump == 'function') {
      dump(text + '\n'); // fast, straight to the real console
    } else {
      console.error(text);
    }
  },
  canvas: (function() {
    var canvas = document.getElementById('canvas');

    // As a default initial behavior, pop up an alert when webgl context is lost. To make your
    // application robust, you may want to override this behavior before shipping!
    // See http://www.khronos.org/registry/webgl/specs/latest/1.0/#5.15.2
    canvas.addEventListener("webglcontextlost", function(e) { alert('WebGL context lost. You will need to reload the page.'); e.preventDefault(); }, false);

    return canvas;
  })(),
  setStatus: function(text) {
    if (!Module.setStatus.last) Module.setStatus.last = { time: Date.now(), text: '' };
    if (text === Module.setStatus.last.text) return;
    var m = text.match(/([^(]+)\((\d+(\.\d+)?)\/(\d+)\)/);
    var now = Date.now();
    if (m && now - Module.setStatus.last.time < 30) return; // if this is a progress update, skip it if too soon
    Module.setStatus.last.time = now;
    Module.setStatus.last.text = text;
    if (m) {
      text = m[1];
      progressElement.value = parseInt(m[2])*100;
      progressElement.max = parseInt(m[4])*100;
      progressElement.hidden = false;
      spinnerElement.hidden = false;
    } else {
      progressElement.value = null;
      progressElement.max = null;
      progressElement.hidden = true;
      if (!text) spinnerElement.style.display = 'none';
    }
    statusElement.innerHTML = text;
  },
  totalDependencies: 0,
  monitorRunDependencies: function(left) {
    this.totalDependencies = Math.max(this.totalDependencies, left);
    Module.setStatus(left ? 'Preparing... (' + (this.totalDependencies-left) + '/' + this.totalDependencies + ')' : 'All downloads complete.');
  }
};
Module.setStatus('Downloading...');
window.onerror = function(event) {
  // TODO: do not warn on ok events like simulating an infinite loop or exitStatus
  Module.setStatus('Exception thrown, see JavaScript console');
  spinnerElement.style.display = 'none';
  Module.setStatus = function(text) {
    if (text) Module.printErr('[post-exception status] ' + text);
  };
};

//InitModule(Module);
</script>
<script  src="<?php echo get_stylesheet_directory_uri(); ?>/resources/EmscriptenSampleOgreCut.js"></script>


		</main><!-- #main -->
	</section><!-- #primary -->
  <?php

if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>